FROM python:3.7.7

ADD requirements.txt /requirements.txt
ADD dist /dist

RUN pip install -r /requirements.txt
RUN pip install /dist/*.whl