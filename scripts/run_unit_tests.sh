#!/bin/bash

CWD=$(pwd)
PYGITLAB_HOME=$(pwd)/..

cd ${PYGITLAB_HOME}
PYTHONPATH=${PYGITLAB_HOME}/src pytest ${PYGITLAB_HOME}/tests/unit
cd ${CWD}