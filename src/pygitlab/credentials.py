import errno
import os
import re

import yaml


class GitLabCredentials:

    ACCESS_TOKEN_REGEX = r"\S{20}"

    def __init__(self, access_token: str):
        self._validate_access_token(access_token=access_token)
        self._access_token = access_token

    @property
    def access_token(self) -> str:
        return self._access_token

    def _validate_access_token(self, access_token: str):
        if access_token is None:
            raise ValueError("Access token cannot be None")
        match = re.fullmatch(self.ACCESS_TOKEN_REGEX, access_token)
        if not match:
            raise ValueError("Access token do not match regex")


class GitLabCredentialsLoader:

    DEFAULT_CONFIG_FILE_NAME = ".pygitlab.yml"

    ENV_VAR__PYGITLAB_CONFIG_FILE_PATH = "PYGITLAB_CONFIG_FILE_PATH"

    @property
    def default_config_file_path(self) -> str:
        return os.path.join(os.environ["HOME"], self.DEFAULT_CONFIG_FILE_NAME)

    def load_from_file(self, config_file_path: str) -> GitLabCredentials:
        if not config_file_path:
            raise ValueError("config_file_path cannot be empty string or None")
        if not os.path.exists(config_file_path):
            raise GitLabConfigNotFound(config_file_path)
        with open(config_file_path, "r") as file:
            config_data = yaml.load(file, Loader=yaml.FullLoader)
        return GitLabCredentials(access_token=config_data["access_token"])

    def load_from_environment(self) -> GitLabCredentials:
        config_file_path = os.environ["PYGITLAB_CONFIG_FILE_PATH"]
        return self.load_from_file(config_file_path)

    def load(self) -> GitLabCredentials:
        if self.ENV_VAR__PYGITLAB_CONFIG_FILE_PATH in os.environ:
            return self.load_from_environment()
        else:
            return self.load_from_file(self.default_config_file_path)


class GitLabConfigNotFound(FileNotFoundError):
    def __init__(self, filepath):
        super().__init__(errno.ENOENT, os.strerror(errno.ENOENT), filepath)