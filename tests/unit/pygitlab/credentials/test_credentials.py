from typing import NamedTuple

import pytest

from pygitlab.credentials import GitLabCredentials


class Data(NamedTuple):
    name: str
    access_token: str


TEST_CASES = [
    Data(
        name="Access token is None",
        access_token=None
    ),
    Data(
        name="Access token is empty",
        access_token=""
    ),
    Data(
        name="Access token is too short",
        access_token="1"
    ),
    Data(
        name="Access token is too long",
        access_token="123456789012345678901"
    ),
    Data(
        name="Access token contains invalid character",
        access_token="1234567890123456789 "
    )
]


@pytest.mark.parametrize(
    argnames="invalid_access_token",
    argvalues=map(lambda test_case: test_case.access_token, TEST_CASES),
    ids=map(lambda test_case: test_case.name, TEST_CASES)
)
def test_credentials__invalid_access_token(invalid_access_token: str):
    with pytest.raises(ValueError):
        GitLabCredentials(access_token=invalid_access_token)


def test_credentials__valid_access_token():
    GitLabCredentials(access_token="12345678901234567890")
