import os

import mock
import pytest

from pygitlab.credentials import GitLabCredentialsLoader, GitLabConfigNotFound

TEST_DATA_ROOT_DIR_PATH = os.path.join("test_data", "unit", "pygitlab", "credentials_loader")

TEST_SIMPLE__CONFIG_FILE_PATH = os.path.join(TEST_DATA_ROOT_DIR_PATH, "simple", ".pygitlab.yml")
TEST_SIMPLE__ACCESS_TOKEN = "01234567890123456789"

TEST_MISSING_FILE__CONFIG_FILE_PATH = os.path.join(TEST_DATA_ROOT_DIR_PATH, "missing_file", ".pygitlab.yml")

TEST_EMPTY_FILE_PATH__CONFIG_FILE_PATH = ""

TEST_ENV_VAR__CONFIG_FILE_PATH = os.path.join(TEST_DATA_ROOT_DIR_PATH, "env_var", ".pygitlab.yml")
TEST_ENV_VAR__ACCESS_TOKEN = "token4environment123"

TEST_ENV_VAR_MISSING_FILE__CONFIG_FILE_PATH = os.path.join(
    TEST_DATA_ROOT_DIR_PATH,
    "env_var__missing_file",
    ".pygitlab.yml"
)

TEST_MOCKED_DEFAULT__HOME_ENV_VAR = os.path.join(TEST_DATA_ROOT_DIR_PATH, "mocked_default")
TEST_MOCKED_DEFAULT__CONFIG_FILE_PATH = os.path.join(
    TEST_DATA_ROOT_DIR_PATH,
    "mocked_default",
    ".pygitlab.yml"
)

TEST_LOADING_CHAIN__ENV_VAR__CONFIG_FILE_PATH = os.path.join(
    TEST_DATA_ROOT_DIR_PATH,
    "loading_chain__env_var",
    ".pygitlab.yml"
)
TEST_LOADING_CHAIN__ENV_VAR__ACCESS_TOKEN = "loadingchainenvvar42"

TEST_LOADING_CHAIN__MOCKED_DEFAULT__HOME_ENV_VAR = os.path.join(
    TEST_DATA_ROOT_DIR_PATH,
    "loading_chain__mocked_default"
)
TEST_LOADING_CHAIN__MOCKED_DEFAULT__ACCESS_TOKEN = "loadingchaindefault1"


def test_credentials_loader__location_from_path__simple():
    credentials_loader = GitLabCredentialsLoader()
    credentials = credentials_loader.load_from_file(
        config_file_path=TEST_SIMPLE__CONFIG_FILE_PATH
    )
    assert credentials.access_token == TEST_SIMPLE__ACCESS_TOKEN


def test_credentials_loader__location_from_path__missing_file():
    credentials_loader = GitLabCredentialsLoader()
    with pytest.raises(GitLabConfigNotFound):
        credentials_loader.load_from_file(config_file_path=TEST_MISSING_FILE__CONFIG_FILE_PATH)


def test_credentials_loader__location_from_path__empty_string():
    credentials_loader = GitLabCredentialsLoader()
    with pytest.raises(ValueError):
        credentials_loader.load_from_file(config_file_path=TEST_EMPTY_FILE_PATH__CONFIG_FILE_PATH)


@mock.patch.dict(
    os.environ,
    {
        "PYGITLAB_CONFIG_FILE_PATH": TEST_ENV_VAR__CONFIG_FILE_PATH
    }
)
def test_credentials_loader__location_from_environment():
    credentials_loader = GitLabCredentialsLoader()
    credentials = credentials_loader.load_from_environment()
    assert credentials.access_token == TEST_ENV_VAR__ACCESS_TOKEN


@mock.patch.dict(
    os.environ,
    {
        "PYGITLAB_CONFIG_FILE_PATH": TEST_ENV_VAR_MISSING_FILE__CONFIG_FILE_PATH
    }
)
def test_credentials_loader__location_from_environment__missing_file():
    credentials_loader = GitLabCredentialsLoader()
    with pytest.raises(GitLabConfigNotFound):
        credentials_loader.load_from_environment()


@mock.patch.dict(
    os.environ,
    {
        "PYGITLAB_CONFIG_FILE_PATH": ""
    }
)
def test_credentials_loader__location_from_environment__empty_string():
    credentials_loader = GitLabCredentialsLoader()
    with pytest.raises(ValueError):
        credentials_loader.load_from_environment()


@mock.patch.dict(
    os.environ,
    {
        "HOME": TEST_MOCKED_DEFAULT__HOME_ENV_VAR
    }
)
def test_credentials_loader__default_config_file_path():
    credentials_loader = GitLabCredentialsLoader()
    assert credentials_loader.default_config_file_path == TEST_MOCKED_DEFAULT__CONFIG_FILE_PATH


@mock.patch.dict(
    os.environ,
    {
        "HOME": TEST_MOCKED_DEFAULT__HOME_ENV_VAR,
        "PYGITLAB_CONFIG_FILE_PATH": TEST_LOADING_CHAIN__ENV_VAR__CONFIG_FILE_PATH
    }
)
def test_credentials_loader__loading_chain__environment_variable_overrides_default_location():
    credentials_loader = GitLabCredentialsLoader()
    credentials = credentials_loader.load()
    assert credentials.access_token == TEST_LOADING_CHAIN__ENV_VAR__ACCESS_TOKEN


@mock.patch.dict(
    os.environ,
    {
        "HOME": TEST_LOADING_CHAIN__MOCKED_DEFAULT__HOME_ENV_VAR,
    }
)
def test_credentials_loader__loading_chain__default_location_if_environment_variable_missing():
    credentials_loader = GitLabCredentialsLoader()
    credentials = credentials_loader.load()
    assert credentials.access_token == TEST_LOADING_CHAIN__MOCKED_DEFAULT__ACCESS_TOKEN
